package com.rvlt.resources;

import com.rvlt.dto.Account;
import com.rvlt.dto.Transfer;
import com.rvlt.dto.User;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.rvlt.resources.mappers.*;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

public class ResourcesTest extends JerseyTest {

    @Override
    protected Application configure() {
        return new ResourceConfig(Users.class, Accounts.class, Transfers.class).register(ExceptionsMapper.class).register(CustomExceptionsMapper.class);
    }

    @Before
    public void setUpTest() throws Exception {
    }

    @After
    public void tearDownTest() throws Exception {
    }


    @Test
    public void testRequestNotExistent() {
        Response output = target("/users/search").queryParam("mail", "qqq@qqq").request().post(null);
        assertEquals("Http Response should be NOTFOUND: ", Response.Status.NOT_FOUND.getStatusCode(), output.getStatus());
        output = target("/transfers/-1").request().get();
        assertEquals("Http Response should be NOTFOUND: ", Response.Status.NOT_FOUND.getStatusCode(), output.getStatus());
    }

    @Test
    public void testDuplicateEmails() {
        User alice = new User(null,"name1","name1","d@d.q");
        User bob = new User(null,"name2","name2","d@d.q");
        Response response = target("/users").request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(alice, MediaType.APPLICATION_JSON_TYPE));
        assertEquals("Http Response should be CREATED ", Response.Status.CREATED.getStatusCode(), response.getStatus());
        response = target("/users").request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(bob, MediaType.APPLICATION_JSON_TYPE));
        assertEquals("Http Response should be CREATED ", Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
    }

    @Test
    public void testTransfer() {
        User alice = new User(null,"alice","alice","alice@alice.q");
        User bob = new User(null,"bob","bob","bob@bob.q");

        //Create users alice and bob. Check get user
        Response response = target("/users").request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(alice, MediaType.APPLICATION_JSON_TYPE));
        assertEquals("Http Response should be CREATED ", Response.Status.CREATED.getStatusCode(), response.getStatus());
        User aliceReturned = response.readEntity(User.class);
        assertEquals(aliceReturned.getEmail(), alice.getEmail());

        response = target("/users").request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(bob, MediaType.APPLICATION_JSON_TYPE));
        assertEquals("Http Response should be CREATED ", Response.Status.CREATED.getStatusCode(), response.getStatus());
        User bobReturned = response.readEntity(User.class);
        assertEquals(bobReturned.getEmail(), bob.getEmail());

        response = target("/users/{id}/accounts").resolveTemplate("id", aliceReturned.getId()).request()
                .get();
        assertEquals("Http Response should be NOTFOUND: ", Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());

        //Create accounts. Check existence.
        Account aliceAcc = new Account(null, "aliceAccount", aliceReturned.getId(), BigDecimal.valueOf(1000));
        response = target("/users/{id}/accounts").resolveTemplate("id", aliceReturned.getId()).request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(aliceAcc, MediaType.APPLICATION_JSON_TYPE));
        assertEquals("Http Response should be CREATED ", Response.Status.CREATED.getStatusCode(), response.getStatus());
        Account aliceAccReturned = response.readEntity(Account.class);
        assertEquals(aliceAcc.getDescription(), aliceAccReturned.getDescription());

        Account bobAcc = new Account(null, "bobAccount", bobReturned.getId(), BigDecimal.valueOf(1000));
        response = target("/users/{id}/accounts").resolveTemplate("id", bobReturned.getId()).request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(bobAcc, MediaType.APPLICATION_JSON_TYPE));
        assertEquals("Http Response should be CREATED ", Response.Status.CREATED.getStatusCode(), response.getStatus());
        Account bobAccReturned = response.readEntity(Account.class);
        assertEquals(bobAcc.getDescription(), bobAccReturned.getDescription());
        //process transfer
        Transfer t = new Transfer(null, "tr1", BigDecimal.valueOf(700), aliceAccReturned.getId(), bobAccReturned.getId());
        response = target("/transfers").request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(t, MediaType.APPLICATION_JSON_TYPE));
        assertEquals("Http Response should be CREATED ", Response.Status.CREATED.getStatusCode(), response.getStatus());
        Transfer tReturned = response.readEntity(Transfer.class);
        assertEquals(t.getNote(), tReturned.getNote());
        //check source
        response = target("/accounts/{id}").resolveTemplate("id", aliceAccReturned.getId()).request()
                .get();
        assertTrue(((new BigDecimal("300.00")).equals(response.readEntity(Account.class).getBalance())));
        //check acceptor
        response = target("/accounts/{id}").resolveTemplate("id", bobAccReturned.getId()).request()
                .get();
        assertTrue(((new BigDecimal("1700.00")).equals(response.readEntity(Account.class).getBalance())));
        //check transfer record count
        response = target("/accounts/{id}/transfers").resolveTemplate("id", aliceAccReturned.getId()).request()
                .get();
        assertTrue(response.readEntity(List.class).size() == 1);
        //process transfer
        Transfer transfer2 = new Transfer(null, "tr2", BigDecimal.valueOf(1000), aliceAccReturned.getId(), bobAccReturned.getId());
        response = target("/transfers").request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(transfer2, MediaType.APPLICATION_JSON_TYPE));
        assertEquals("Http Response should be CREATED ", Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        ErrorDetails transfer2Res = response.readEntity(ErrorDetails.class);
        assertThat(transfer2Res.getErrormsg(), CoreMatchers.containsString("Insufficient funds"));
    }


}
