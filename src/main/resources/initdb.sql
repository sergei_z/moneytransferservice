--DROP TABLE IF EXISTS Account;
--DROP TABLE IF EXISTS User;
--DROP TABLE IF EXISTS Transfer;

CREATE TABLE IF NOT EXISTS USER
(
    userid    LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
    firstname VARCHAR(150)                    NOT NULL,
    lastname  VARCHAR(150)                    NOT NULL,
    email     VARCHAR(100)                   unique NOT NULL
);
CREATE TABLE IF NOT EXISTS ACCOUNT
(
    accountid   LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
    description VARCHAR(30),
    owner       LONG,
    balance     DECIMAL(20, 2),
    foreign key (owner) references USER (userid)
);
CREATE TABLE IF NOT EXISTS TRANSFER
(
    transferid LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
    note       VARCHAR(100),
    amount     DECIMAL(20, 2),
    source     LONG,
    acceptor   LONG,
    foreign key (source) references ACCOUNT (accountid),
    foreign key (acceptor) references ACCOUNT (accountid)
);