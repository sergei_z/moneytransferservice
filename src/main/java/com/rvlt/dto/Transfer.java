package com.rvlt.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Transfer {
    private Long id;
    private String note;
    private BigDecimal amount;
    private Long source;
    private Long acceptor;

    public Long getId() {
        return id;
    }

    public Transfer() {
    }

    public Transfer(Long id, String note, BigDecimal amount, Long source, Long acceptor) {
        this.id = id;
        this.note = note;
        this.amount = amount;
        this.source = source;
        this.acceptor = acceptor;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getSource() {
        return source;
    }

    public void setSource(Long source) {
        this.source = source;
    }

    public Long getAcceptor() {
        return acceptor;
    }

    public void setAcceptor(Long acceptor) {
        this.acceptor = acceptor;
    }
}
