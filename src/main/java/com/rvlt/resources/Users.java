package com.rvlt.resources;

import com.rvlt.dao.AccountDAO;
import com.rvlt.dao.UserDAO;
import com.rvlt.dto.Account;
import com.rvlt.dto.User;
import com.rvlt.exceptions.TransferServiceExceprion;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * users root resource
 */
@Path("users")
public class Users {
    UserDAO userDAO = new UserDAO();
    AccountDAO accountDAO = new AccountDAO();

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@PathParam("id") Integer id) throws TransferServiceExceprion {
        return Response.ok(userDAO.getUserById(id)).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers() throws TransferServiceExceprion {
        return Response.ok(userDAO.getAllUsers()).build();

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createUser(User user) throws TransferServiceExceprion {
        Long id = userDAO.insertUser(user);
        return Response.status(Response.Status.CREATED).entity(userDAO.getUserById(id)).build();
    }

    @POST
    @Path("search")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findUserByEmail(@QueryParam("mail") String mail) throws TransferServiceExceprion {
        return Response.ok(userDAO.getUserByEmail(mail)).build();

    }


    @GET
    @Path("{id}/accounts")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserAccounts(@PathParam("id") Long id) throws TransferServiceExceprion {
        return Response.ok(accountDAO.getUserAccounts(id)).build();

    }


    @POST
    @Path("{id}/accounts")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUserAccount(@PathParam("id") Long id, Account account) throws TransferServiceExceprion {
        Long accountId = accountDAO.insertUserAccount(id, account);
        return Response.status(Response.Status.CREATED).entity(accountDAO.getAccountById(accountId)).build();
    }

}
