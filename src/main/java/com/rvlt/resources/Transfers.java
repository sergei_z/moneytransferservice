package com.rvlt.resources;

import com.rvlt.dao.TransferDAO;
import com.rvlt.dto.Transfer;
import com.rvlt.exceptions.TransferServiceExceprion;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Root resource transactions
 */
@Path("transfers")
public class Transfers {
    TransferDAO transferDAO = new TransferDAO();

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTransaction(@PathParam("id") Long id) throws TransferServiceExceprion {
        return Response.ok(transferDAO.getTransfer(id)).build();

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response processNewTransfer(Transfer t) throws TransferServiceExceprion {
        Long transferId = transferDAO.processTransfer(t);
        return Response.status(Response.Status.CREATED).entity(transferDAO.getTransfer(transferId)).build();
    }


}
