package com.rvlt.resources.mappers;

import com.rvlt.exceptions.DAOException;
import com.rvlt.exceptions.TransferServiceExceprion;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ExceptionsMapper implements ExceptionMapper<Throwable> {
    @Override
    public Response toResponse(Throwable e) {
        {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorDetails(e.getMessage(), e.getCause()!=null? e.getCause().getMessage(): "N/A")).type(MediaType.APPLICATION_JSON).build();
        }
    }
}
