package com.rvlt.resources.mappers;

public class ErrorDetails {
    public ErrorDetails() {
    }

    public ErrorDetails(String errormsg, String causemsg) {
        this.errormsg = errormsg;
        this.causemsg = causemsg;
    }

    String errormsg;
    String causemsg;

    public String getCausemsg() {
        return causemsg;
    }

    public void setCausemsg(String causemsg) {
        this.causemsg = causemsg;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public void setErrormsg(String errormsg) {
        this.errormsg = errormsg;
    }
}
