package com.rvlt.resources.mappers;

import com.rvlt.exceptions.DAOException;
import com.rvlt.exceptions.ObjectNotFoundException;
import com.rvlt.exceptions.TransferServiceExceprion;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class CustomExceptionsMapper implements ExceptionMapper<TransferServiceExceprion> {

    @Override
    public Response toResponse(TransferServiceExceprion e) {
        if (e instanceof ObjectNotFoundException) {
            return Response.status(Response.Status.NOT_FOUND).entity(new ErrorDetails(e.getMessage(), "")).build();
        } else if (e instanceof DAOException) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorDetails(e.getMessage(), e.getCause() != null ? e.getCause().getMessage() : "N/A")).type(MediaType.APPLICATION_JSON).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorDetails(e.getMessage(), e.getCause() != null ? e.getCause().getMessage() : "N/A")).type(MediaType.APPLICATION_JSON).build();
        }
    }
}
