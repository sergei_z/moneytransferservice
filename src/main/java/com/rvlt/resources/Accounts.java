package com.rvlt.resources;

import com.rvlt.dao.AccountDAO;
import com.rvlt.dao.TransferDAO;
import com.rvlt.dao.UserDAO;
import com.rvlt.dto.Account;
import com.rvlt.dto.Transfer;
import com.rvlt.dto.User;
import com.rvlt.exceptions.TransferServiceExceprion;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.List;

/**
 * Root resource accounts
 */
@Path("accounts")
public class Accounts {
    UserDAO userDAO = new UserDAO();
    AccountDAO accountDAO = new AccountDAO();
    TransferDAO transferDAO = new TransferDAO();

    /**
     * Get account specified by id
     *
     * @param id Account ID
     * @return Account
     */
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccount(@PathParam("id") Long id) {
        try {
            Account a = accountDAO.getAccountById(id);
            if (a != null) {
                return Response.ok(a).build();
            } else
                return Response.status(Response.Status.NOT_FOUND).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Get accounts
     *
     * @return Account
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllAccounts() {
        try {
            List<Account> a = accountDAO.getAllAccounts();
            if (a != null && !a.isEmpty()) {
                return Response.ok(a).build();
            } else
                return Response.status(Response.Status.NOT_FOUND).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }


    /**
     * Get account transactions
     *
     * @param id Account ID
     * @return Transfer list
     */
    @GET
    @Path("{id}/transfers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccountTransactions(@PathParam("id") Long id) {
        try {
            List<Transfer> transfers = transferDAO.getAccountTransfers(id);
            if (transfers != null && !transfers.isEmpty()) {
                return Response.ok(transfers).build();
            } else
                return Response.status(Response.Status.NOT_FOUND).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Update account balance
     * @param id Account ID
     * @param amount Amount
     * @return Updated account object
     * @throws TransferServiceExceprion
     */
    @POST
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateBalance(@PathParam("id") Long id, @QueryParam("amount") BigDecimal amount) throws TransferServiceExceprion {
        accountDAO.updateAccountBalance(id, amount);
        return Response.status(Response.Status.CREATED).entity(accountDAO.getAccountById(id)).build();
    }

}
