package com.rvlt.dao;


import com.rvlt.dto.Account;
import com.rvlt.exceptions.BusinessException;
import com.rvlt.exceptions.DAOException;
import com.rvlt.exceptions.ObjectNotFoundException;
import com.rvlt.exceptions.TransferServiceExceprion;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class AccountDAO extends CommonDAO {
    private static Logger logger = Logger.getLogger(AccountDAO.class);

    public Long insertUserAccount(Long userId, Account account) throws DAOException {
        try (Connection connection = getConnection(); PreparedStatement statement = connection.prepareStatement(SQL_INSERT_ACCOUNT);) {
            statement.setString(1, account.getDescription());
            statement.setLong(2, account.getOwner());
            statement.setBigDecimal(3, account.getBalance());
            statement.executeUpdate();
            try (ResultSet generatedKey = statement.getGeneratedKeys()) {
                if (generatedKey.next()) {
                    return generatedKey.getLong(1);
                } else {
                    logger.error("Create account failed");
                    throw new DAOException("Create account failed");
                }
            }
        } catch (SQLException e) {
            logger.error("Create account failed", e);
            throw new DAOException("Create account failed", e);
        }
    }

    public List<Account> getAllAccounts() throws TransferServiceExceprion {
        List<Account> accounts = new ArrayList<>();
        try (Connection connection = getConnection(); PreparedStatement statement = connection.prepareStatement(SQL_GET_ALL_ACCOUNTS); ResultSet rs = statement.executeQuery();) {
            while (rs.next()) {
                accounts.add(new Account(rs.getLong("accountid"), rs.getString("description"), rs.getLong("owner"), rs.getBigDecimal("balance")));
            }
            if (accounts.size() > 0) {
                return accounts;
            } else {
                throw new ObjectNotFoundException("Account not found");
            }

        } catch (SQLException e) {
            logger.error("get all accounts failed", e);
            throw new DAOException("get all accounts failed", e);
        }
    }

    public Account getAccountById(Long id) throws TransferServiceExceprion {
        Account account = null;
        try (Connection connection = getConnection(); PreparedStatement stmt = connection.prepareStatement(SQL_GET_ACCOUNT_BY_ID)) {
            stmt.setLong(1, id);
            try (ResultSet rs = stmt.executeQuery();) {
                if (rs.next()) {
                    return new Account(rs.getLong("accountid"), rs.getString("description"), rs.getLong("owner"), rs.getBigDecimal("balance"));
                } else {
                    throw new ObjectNotFoundException("Account not found");
                }
            }
        } catch (SQLException e) {
            throw new DAOException("get account by id failed", e);
        }
    }

    public List<Account> getUserAccounts(Long userId) throws TransferServiceExceprion {
        List<Account> accounts = new ArrayList<>();
        try (Connection connection = getConnection(); PreparedStatement stmt = connection.prepareStatement(SQL_GET_ACCOUNT_BY_USER_ID)) {
            stmt.setLong(1, userId);
            try (ResultSet rs = stmt.executeQuery();) {
                while (rs.next()) {
                    accounts.add(new Account(rs.getLong("accountid"), rs.getString("description"), rs.getLong("owner"), rs.getBigDecimal("balance")));
                }
                if (accounts.size() > 0) {
                    return accounts;
                } else {
                    throw new ObjectNotFoundException("Account not found");
                }
            }
        } catch (SQLException e) {
            throw new DAOException("get accounts by user failed", e);
        }
    }

    public void updateAccountBalance(long id, BigDecimal deltaAmount) throws TransferServiceExceprion {
        Connection connection = null;
        PreparedStatement statementLock = null;
        PreparedStatement statementUpdate = null;
        ResultSet resultSet = null;
        Account targetAccount = null;
        int updateCount = 0;
        try {
            connection = getConnection();
            connection.setAutoCommit(false);
            //Phase 1. lock
            statementLock = connection.prepareStatement(SQL_GET_ACCOUNT_FOR_UPDATE);
            statementLock.setLong(1, id);
            resultSet = statementLock.executeQuery();
            if (resultSet.next()) {
                targetAccount = new Account(resultSet.getLong("accountid"), resultSet.getString("description"), resultSet.getLong("owner"), resultSet.getBigDecimal("balance"));
            } else {
                throw new BusinessException("This account is not available now. " + id);
            }
            BigDecimal balance = targetAccount.getBalance().add(deltaAmount);
            if (balance.compareTo(BigDecimal.ZERO) < 0) {
                throw new BusinessException("Change balance operation failed (Insufficient funds)");
            }
            //Phase 2. update
            statementUpdate = connection.prepareStatement(SQL_UPDATE_BALANCE);
            statementUpdate.setBigDecimal(1, balance);
            statementUpdate.setLong(2, id);
            if (statementUpdate.executeUpdate() < 1) {
                throw new DAOException("Change balance operation failed (update failure)");
            }
            connection.commit();
        } catch (SQLException e) {
            try {
                if (connection != null)
                    connection.rollback();
            } catch (SQLException e1) {
                throw new DAOException("Change balance transaction rollback failed", e1);
            }
            logger.error("Change balance operation failed for account " + id, e);
            throw new DAOException("Change balance operation failed for account " + id, e);
        } finally {
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                statementUpdate.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                statementLock.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}