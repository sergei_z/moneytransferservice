package com.rvlt.dao;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

class CommonDAO {
    static Logger log = Logger.getLogger(CommonDAO.class);
    final static String SQL_GET_ACCOUNT_FOR_UPDATE = "SELECT * FROM ACCOUNT WHERE accountid = ? FOR UPDATE";
    final static String SQL_UPDATE_BALANCE = "UPDATE ACCOUNT SET balance = ? WHERE accountid = ? ";
    final static String SQL_GET_ACCOUNT_TRANSFERS = "SELECT * FROM TRANSFER WHERE source = ? OR acceptor = ?";
    final static String SQL_INSERT_TRANSFER = "INSERT INTO TRANSFER (note, amount, source, acceptor) VALUES (?, ?, ? ,?)";
    final static String SQL_GET_TRANSFER_BY_ID = "SELECT * FROM TRANSFER WHERE transferid = ? ";
    final static String SQL_GET_USER_BY_ID = "SELECT * FROM USER WHERE userid = ? ";
    final static String SQL_GET_USERS = "SELECT * FROM USER";
    final static String SQL_GET_USER_BY_EMAIL = "SELECT * FROM USER WHERE email = ? ";
    final static String SQL_INSERT_USER = "INSERT INTO USER (firstname, lastname, email) VALUES (?, ?, ?)";
    final static String SQL_GET_ACCOUNT_BY_ID = "SELECT * FROM ACCOUNT WHERE accountid = ? ";
    final static String SQL_GET_ACCOUNT_BY_USER_ID = "SELECT * FROM ACCOUNT WHERE owner = ? ";
    final static String SQL_INSERT_ACCOUNT = "INSERT INTO ACCOUNT (description, owner, balance) VALUES (?, ?, ?)";
    final static String SQL_GET_ALL_ACCOUNTS = "SELECT * FROM ACCOUNT";
    private static Properties properties = new Properties();


    public static void loadProperties(String file) {
        try {
            properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(file));
        } catch (Exception e) {
            e.printStackTrace();
            log.fatal("Properties loading error", e);
        }
    }

    static {
        loadProperties("db.properties");
        try {
            Class.forName(getStringProperty("DRIVER"));
        } catch (ClassNotFoundException e) {
            log.fatal("Driver loading error", e);
        }
    }

    public static String getStringProperty(String key) {
        return properties.getProperty(key);
    }


    Connection getConnection() throws SQLException {
        return DriverManager.getConnection(getStringProperty("URL"), getStringProperty("LOGIN"), getStringProperty("PASSWORD"));
    }


    public void populateTestData() {
        /*log.info("Populating Test User Table and data ..... ");
        Connection conn = null;
        try {
            conn = CommonDAO.getConnection();
            RunScript.execute(conn, new FileReader("src/test/resources/demo.sql"));
        } catch (SQLException e) {
            log.error("populateTestData(): Error populating user data: ", e);
            throw new RuntimeException(e);
        } catch (FileNotFoundException e) {
            log.error("populateTestData(): Error finding test script file ", e);
            throw new RuntimeException(e);
        } finally {
            DbUtils.closeQuietly(conn);
        }*/
    }

}
