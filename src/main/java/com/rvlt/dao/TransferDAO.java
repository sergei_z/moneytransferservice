package com.rvlt.dao;

import com.rvlt.dto.Account;
import com.rvlt.dto.Transfer;
import com.rvlt.exceptions.BusinessException;
import com.rvlt.exceptions.DAOException;
import com.rvlt.exceptions.ObjectNotFoundException;
import com.rvlt.exceptions.TransferServiceExceprion;
import org.apache.log4j.Logger;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TransferDAO extends CommonDAO {
    private static Logger logger = Logger.getLogger(TransferDAO.class);

    public Transfer getTransfer(Long id) throws TransferServiceExceprion {
        Transfer transfer = null;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_GET_TRANSFER_BY_ID);) {
            statement.setLong(1, id);
            try (ResultSet rs = statement.executeQuery();) {
                if (rs.next()) {
                    return new Transfer(rs.getLong("transferid"), rs.getString("note"), rs.getBigDecimal("amount"), rs.getLong("source"), rs.getLong("acceptor"));
                } else {
                    throw new ObjectNotFoundException("Transfer not found");
                }

            }
        } catch (SQLException e) {
            logger.error("Get transfer failed", e);
            throw new DAOException("Get transfer failed", e);
        }
    }

    public Long processTransfer(Transfer transfer) throws TransferServiceExceprion {
        Connection connection = null;
        PreparedStatement statementLock = null;
        PreparedStatement statementUpdate = null;
        ResultSet resultSet = null;
        Account source = null;
        Account acceptor = null;
        Long ret = 0L;
        try {
            connection = getConnection();
            connection.setAutoCommit(false);
            //lock and check
            statementLock = connection.prepareStatement(SQL_GET_ACCOUNT_FOR_UPDATE);
            statementLock.setLong(1, transfer.getSource());
            resultSet = statementLock.executeQuery();
            if (resultSet.next()) {
                source = new Account(resultSet.getLong("accountid"), resultSet.getString("description"),  resultSet.getLong("owner"), resultSet.getBigDecimal("balance"));
            } else {
                logger.warn("Transfer failed. Source account is locked ");
                throw new BusinessException("Transfer failed. Source account is locked " + transfer.getSource());
            }
            if (source.getBalance().subtract(transfer.getAmount()).compareTo(BigDecimal.ZERO) < 0) {
                logger.warn("Transfer operation failed (Insufficient funds)");
                throw new BusinessException("Transfer operation failed (Insufficient funds)");
            }

            statementLock = connection.prepareStatement(SQL_GET_ACCOUNT_FOR_UPDATE);
            statementLock.setLong(1, transfer.getAcceptor());
            resultSet = statementLock.executeQuery();
            if (resultSet.next()) {
                acceptor = new Account(resultSet.getLong("accountid"), resultSet.getString("description"), resultSet.getLong("owner"), resultSet.getBigDecimal("balance"));
            } else {
                logger.warn("Transfer failed. Acceptor account is locked ");
                throw new BusinessException("Transfer failed. Acceptor account is locked " + transfer.getAcceptor());
            }
            //transfer
            statementUpdate = connection.prepareStatement(SQL_UPDATE_BALANCE);
            statementUpdate.setBigDecimal(1, source.getBalance().subtract(transfer.getAmount()));
            statementUpdate.setLong(2, transfer.getSource());
            statementUpdate.addBatch();
            statementUpdate.setBigDecimal(1, acceptor.getBalance().add(transfer.getAmount()));
            statementUpdate.setLong(2, transfer.getAcceptor());
            statementUpdate.addBatch();
            int[] rowsUpdated = statementUpdate.executeBatch();
            statementUpdate = connection.prepareStatement(SQL_INSERT_TRANSFER, statementUpdate.RETURN_GENERATED_KEYS);
            statementUpdate.setString(1, transfer.getNote());
            statementUpdate.setBigDecimal(2, transfer.getAmount());
            statementUpdate.setLong(3, transfer.getSource());
            statementUpdate.setLong(4, transfer.getAcceptor());
            statementUpdate.executeUpdate();
            try (ResultSet generatedKeys = statementUpdate.getGeneratedKeys()){
                if (generatedKeys.next()){
                    ret = generatedKeys.getLong(1);
                }
            }
            connection.commit();
            return ret;
        } catch (SQLException e) {
            logger.error("Transfer operation failed");
            try {
                if (connection != null)
                    connection.rollback();
            } catch (SQLException e1) {
                logger.fatal("Fail to rollback transaction", e1);
                throw new DAOException("Fail to rollback transaction", e1);
            }
            logger.fatal("Transfer operation failed", e);
            throw new DAOException("Transfer operation failed", e);
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (statementUpdate != null)
                    statementUpdate.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (statementLock != null)
                    statementLock.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    public List<Transfer> getAccountTransfers(Long accountId) throws TransferServiceExceprion {
        List<Transfer> transfers = new ArrayList<>();
        try (Connection connection = getConnection(); PreparedStatement statement = connection.prepareStatement(SQL_GET_ACCOUNT_TRANSFERS);) {
            statement.setLong(1, accountId);
            statement.setLong(2, accountId);
            try (ResultSet rs = statement.executeQuery();) {
                while (rs.next()) {
                    transfers.add(new Transfer(rs.getLong("transferid"), rs.getString("note"), rs.getBigDecimal("amount"), rs.getLong("source"), rs.getLong("acceptor")));
                }
                if (transfers.size() > 0) {
                    return transfers;
                } else {
                    throw new ObjectNotFoundException("Users not found");
                }
            }
        } catch (SQLException e) {
            logger.error("Get users failed", e);
            throw new DAOException("Get users failed", e);
        }
    }
}
