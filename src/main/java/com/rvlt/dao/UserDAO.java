package com.rvlt.dao;


import com.rvlt.dto.User;
import com.rvlt.exceptions.DAOException;
import com.rvlt.exceptions.ObjectNotFoundException;
import com.rvlt.exceptions.TransferServiceExceprion;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class UserDAO extends CommonDAO {

    private static Logger log = Logger.getLogger(UserDAO.class);

    /**
     * Get list of all registered users
     *
     * @return user list
     * @throws DAOException
     */
    public List<User> getAllUsers() throws TransferServiceExceprion {
        List<User> users = new ArrayList<>();
        try (Connection connection = getConnection(); PreparedStatement statement = connection.prepareStatement(SQL_GET_USERS);) {
            try (ResultSet rs = statement.executeQuery();) {
                while (rs.next()) {
                    users.add(new User(rs.getLong("userid"), rs.getString("firstname"), rs.getString("lastname"), rs.getString("email")));
                }
                if (users.size() > 0) {
                    return users;
                } else {
                    throw new ObjectNotFoundException("Users not found");
                }
            }
        } catch (SQLException e) {
            log.error("Get users failed", e);
            throw new DAOException("Get users failed", e);
        }
    }

    /**
     * Get user specified by email. Email is a kind of unique identifier.
     *
     * @param email
     * @return
     * @throws DAOException
     */
    public User getUserByEmail(String email) throws TransferServiceExceprion {
        User u = null;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_GET_USER_BY_EMAIL);) {
            statement.setString(1, email);
            try (ResultSet rs = statement.executeQuery();) {
                if (rs.next()) {
                    return new User(rs.getLong("userid"), rs.getString("firstname"), rs.getString("lastname"), rs.getString("email"));
                } else
                    throw new ObjectNotFoundException("User not found");
            }
        } catch (SQLException e) {
            log.error("Get user by email failed", e);
            throw new DAOException("Get user by email failed", e);
        }
    }

    public User getUserById(long id) throws TransferServiceExceprion {
        User u = null;
        try (Connection connection = getConnection(); PreparedStatement statement = connection.prepareStatement(SQL_GET_USER_BY_ID);) {
            statement.setLong(1, id);
            try (ResultSet rs = statement.executeQuery();) {
                if (rs.next()) {
                    return new User(rs.getLong("userid"), rs.getString("firstname"), rs.getString("lastname"), rs.getString("email"));
                } else {
                    throw new ObjectNotFoundException("User not found");
                }
            }
        } catch (SQLException e) {
            log.error("Get user failed", e);
            throw new DAOException("Get user failed", e);
        }
    }


    public Long insertUser(User user) throws DAOException {
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS);
        ) {
            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getLastName());
            statement.setString(3, user.getEmail());
            statement.executeUpdate();
            try (ResultSet generatedKeys = statement.getGeneratedKeys();) {
                if (generatedKeys.next()) {
                    return generatedKeys.getLong(1);
                } else {
                    throw new DAOException("Insert user failed");
                }
            }
        } catch (SQLException e) {
            log.error("Create user failed" + user, e);
            throw new DAOException("Create user failed", e);
        }

    }

}
