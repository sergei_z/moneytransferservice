package com.rvlt.exceptions;

public class DAOException extends TransferServiceExceprion {
    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }
}
