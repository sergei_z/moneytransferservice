package com.rvlt.exceptions;

public class BusinessException extends TransferServiceExceprion {
    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }
}
