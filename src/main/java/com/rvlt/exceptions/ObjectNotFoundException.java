package com.rvlt.exceptions;

public class ObjectNotFoundException extends TransferServiceExceprion{

    public ObjectNotFoundException(String message) {
        super(message);
    }

    public ObjectNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
